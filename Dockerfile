FROM maven:3.5.2-jdk-8-alpine

RUN mvn --version ; hostname -f

FROM openjdk:8

RUN java -version ; hostname -f